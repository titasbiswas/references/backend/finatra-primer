package app.controller

import app.model.Post
import app.service.PostService
import com.google.inject.Inject
import com.jakehschwartz.finatra.swagger.SwaggerController
import com.twitter.finagle.http.Request
import io.swagger.models.Swagger

class PostResourceWithDoc @Inject()(s: Swagger, postService: PostService) extends SwaggerController {

  implicit protected val swagger = s

  getWithDoc("/posts/withdoc/:user") { o =>
    o.summary("Read all the posts for a user")
      .tag("Posts")
      .routeParam[String]("user", "the user identifier")
      .responseWith[List[Post]](200, "the posts")
      .responseWith(404, "the user not found")
  } { request: Request =>
    postService.findPost(request.getParam("user"))
  }

 /* postWithDoc("posts/withdoc") {
    _.summary("Add a post for the user")
      .tag("Posts")
      .bodyParam[Post]("body")
      .responseWith[Unit](201, "new post added")
      .responseWith(404, "user not found")
      { post: Post =>
        val addedPost = postService.addPost(post)
        response.created.location(s"/posts/${addedPost.user}")
      }
  }*/

}
