package app.controller

import app.model.Post
import app.service.PostService
import com.google.inject.Inject
import com.twitter.finagle.http.Request
import com.twitter.finatra.http.Controller
import com.twitter.inject.Logging

class PostResource @Inject()(postService: PostService) extends Controller with Logging {


  post("/posts") { post: Post =>
    val t = time(s"Time taken to post for user ${post.user} is %d ms") {
      val addedPost = postService.addPost(post)
      response.created.location(s"/posts/${addedPost.user}")
    }
    t
  }

  get("/posts/:user") { request: Request =>
    val userIdentifier = request.getParam("user")
    info(s"Finding posts for the user $userIdentifier")
    postService.findPost(userIdentifier)
  }
}
