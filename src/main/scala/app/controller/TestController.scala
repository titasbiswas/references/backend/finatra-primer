package app.controller

import com.twitter.finagle.http.Request
import com.twitter.finatra.http.Controller

class TestController extends Controller{

  get("/hello"){request: Request =>
    "Hello There"
  }
}
