package app.model

import java.time.Instant

import com.twitter.finatra.validation.constraints.Size

case class Post(@Size(min = 3, max = 10) user: String,
                postText: String,
                hastag: Option[String],
                postedAt: Instant = Instant.now())
