package app.service

import app.model.Post
import com.google.inject.Singleton

import scala.collection.mutable

@Singleton
class PostService {

  val postDb = mutable.Map[String, List[Post]]()

  def addPost(post: Post): Post = {
    val userPosts = postDb.get(post.user) match {
      case Some(posts) => posts :+ post
      case None => List(post)
    }
    postDb.put(post.user, userPosts)
    post
  }

  def findPost(user: String): List[Post] =
    postDb.getOrElse(user, List())


}
