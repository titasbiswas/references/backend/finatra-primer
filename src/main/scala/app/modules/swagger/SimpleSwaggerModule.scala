package app.modules.swagger

import com.google.inject.{Provides, Singleton}
import com.jakehschwartz.finatra.swagger.SwaggerModule
import io.swagger.models.auth.BasicAuthDefinition
import io.swagger.models.{Info, Swagger}

object SimpleSwaggerModule extends SwaggerModule {
  @Singleton
  @Provides
  def swagger: Swagger = {
    val swagger = new Swagger()

    val info = new Info()
      .description("This is a sample for swagger document generation")
      .version("1.0")
      .title("Users Post Management API")

    swagger
      .info(info)
      .addSecurityDefinition("sampleBasic", {
        val d = new BasicAuthDefinition()
        d.setType("basic")
        d
      })

    swagger
  }
}
