package app

import app.controller.{PostResource, PostResourceWithDoc, TestController}
import app.modules.swagger.SimpleSwaggerModule
import com.jakehschwartz.finatra.swagger.DocsController
import com.twitter.finatra.http.HttpServer
import com.twitter.finatra.http.filters.CommonFilters
import com.twitter.finatra.http.routing.HttpRouter

object PersonApp extends PersonServer

class PersonServer extends HttpServer {

  override protected def defaultHttpPort: String = ":8180"

  override protected def defaultHttpServerName: String = "MySocialApp"

  override protected def modules = Seq(SimpleSwaggerModule) // Use the swagger module

  override protected def configureHttp(router: HttpRouter): Unit = {
    router
      .filter[CommonFilters] // Added  for working with common validators, Check the class 'Posts' for validation annotation (@Size)
      .add[TestController]
      .add[DocsController] //Added for use the swagger module
      .add[PostResource]
      .add[PostResourceWithDoc]
  }
}
